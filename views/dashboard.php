<?php
defined( 'ABSPATH' ) or die( 'Cheating, huh?' );
global $zeffl_version, $forms_table, $submissions_table, $views_table, $wpdb;
$forms = $wpdb->get_results( "SELECT id,name,modified FROM $forms_table" );
$time = date('Y-m-d 00:00:00',time()+zeffl_offset());

$total_subs = $wpdb->get_var( "SELECT COUNT(*) FROM $submissions_table" );
$today_subs = $wpdb->get_var( "SELECT COUNT(*) FROM $submissions_table WHERE created > '$time'" );
$today_views = $wpdb->get_var( "SELECT SUM(views) FROM $views_table WHERE views_date = '$time'" );

$today_views = $today_views==null?0:$today_views;
$today_subs = $today_subs==null?0:$today_subs;
$total_subs = $total_subs==null?0:$total_subs;
$total_forms = count($forms)==0 ? '0' : count($forms);
?>
<style>
	#toast-container
	{
		top: 10px;
	}
</style>
<div class='zeffl-forms-css'>

	<div class='row'>
		<div class='large-12 column zeffl-brand-header '>
			<div class="content">
				<div class="sub-header">Zettl forms</div>
				<div class="">
					<button data-toggle="zefflmodal" data-target="#new_form_modal" class="button">New form</button>
				</div>
			</div>	
		</div>
	</div>
	<div class='row' style='position: relative; z-index: 101'>
		<div class='large-12 column'>
			<div id='form_options'>
				<div class='row zeffl-header'>
					<div class='large-4 column zeffl-header-item' style='color: inherit'>
						<div class="row display-flex align__middle bg-gradient-blue">
							<div class="large-6 column">
								<span class='two'><?php _e('form views','zeffl_basic'); ?></span>
								<span class='three'><?php _e('today','zeffl_basic'); ?></span>
							</div>
							<div class="large-6 column">
								<span class='one' style='border-color: inherit'><?php echo $today_views; ?></span>
							</div>
						</div>
					</div>
					<div class='large-4 column zeffl-header-item' style='color: inherit'>
						<div class="row display-flex align__middle bg-gradient-teal">
								<div class="large-6 column">
									<span class='two'><?php _e('submissions','zeffl_basic'); ?></span>
									<span class='three'><?php _e('today','zeffl_basic'); ?></span>
								</div>
								<div class="large-6 column">
								<span class='one' style='border-color: inherit'><?php echo $today_subs; ?></span>
								</div>
						</div>
					</div>
					<div class='large-4 column zeffl-header-item' style='color: inherit'>
						<div class="row display-flex align__middle bg-gradient-purple">
									<div class="large-6 column">
										<span class='two'><?php _e('total','zeffl_basic'); ?></span>
										<span class='three'><?php _e('submissions','zeffl_basic'); ?></span>
									</div>
									<div class="large-6 column">
									<span class='one' style='border-color: inherit'><?php echo $total_subs; ?></span>
									</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> 
	<div class='row'>
		<div class='column subs_cover'>
			<h2><?php _e('Form Submissions','zeffl_basic'); ?> <span id='total-submissions'></span></h2>
			<div class='block'>
				<div class='table_list subs_list <?php echo $total_subs==0?'no-subs':''; ?>' cellpadding='0' cellspacing='0'>
					<div class='loader'>
					</div>
					<div class='tr thead'>
						<!-- <span style='width:8.5%'><label><input class='subs_checked_parent' name='subs_checked_parent' type='checkbox'></label></span> -->
						<div class="column large-6">
							<select id='which-form'>
								<option value='0'><?php _e('All Forms','zeffl_basic'); ?></option>
								<?php
								foreach ($forms as $key => $value) {
									echo "<option value='".$value->id."'>".$value->name."</option>";
								}
								?>
							</select>
						</div>

						<div class="column large-6"><p>Received</p></div>
					</div>

					<div class='tbody'></div>
					<div class='no-subs-content'><?php _e('No Submissions','zeffl_basic'); ?></div>
					<?php if ($total_subs!=0) { ?>
						<div class='pagination'>
							<span>1</span>
						</div>
					<?php }?>
				</div>
			</div>
		</div>
	</div>
	<div class="zefflmodal zefflfade" id="new_form_modal">
		<div class="zefflmodal-dialog" style="width: 350px">
			<form class="zefflmodal-content" id='new_form'>
			<div class='zefflmodal-header'>
					<button class='zefflclose' type="button" class="close" data-dismiss="zefflmodal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="active"><?php _e('New Form','zeffl_basic'); ?></span>
					<nav class='nav-tabs' data-content='#add_form_tabs'>
						

					</nav>
			</div>
				<div class="zefflmodal-body">
					<input type='text' name='form_name' placeholder='<?php _e('Form Name','zeffl_basic'); ?>'>

					<div class="zefflmodal-section">
						<button type="submit" class="button blue wide"><span><?php _e('Create Form','zeffl_basic'); ?></span><span class="zeffl-spinner small">
							<span class="bounce1"></span>
							<span class="bounce2"></span>
							<span class="bounce3"></span>
						</span></button>
						<span class='response'></span>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="zefflmodal zefflfade" id="submission_modal">
		<div class="zefflmodal-dialog">
			<div class="zefflmodal-content">
				<div class="zefflmodal-header">
					<button class='zefflclose' type="button" class="close" data-dismiss="zefflmodal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="zefflmodal-title"></h4>
				</div>
				<div class="zefflmodal-body">
					BODY
				</div>
				<div class="zefflmodal-footer">
					<button class="button blue small close"><?php _e('Close','zeffl_basic'); ?></button>
				</div>
			</div>
		</div>
	</div>

</div>