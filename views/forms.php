<?php
defined( 'ABSPATH' ) or die( 'Cheating, huh?' );
global $zeffl_version, $forms_table, $submissions_table, $views_table, $wpdb;
$forms = $wpdb->get_results( "SELECT id,name,modified FROM $forms_table" );
$time = date('Y-m-d 00:00:00',time()+zeffl_offset());

$total_subs = $wpdb->get_var( "SELECT COUNT(*) FROM $submissions_table" );
$today_subs = $wpdb->get_var( "SELECT COUNT(*) FROM $submissions_table WHERE created > '$time'" );
$today_views = $wpdb->get_var( "SELECT SUM(views) FROM $views_table WHERE views_date = '$time'" );

$today_views = $today_views==null?0:$today_views;
$today_subs = $today_subs==null?0:$today_subs;
$total_subs = $total_subs==null?0:$total_subs;
$total_forms = count($forms)==0 ? '0' : count($forms);
?>
<style>
	#toast-container
	{
		top: 10px;
	}
</style>
<div class='zeffl-forms-css'>

	<div class='row'>
		<div class='large-12 column zeffl-brand-header '>
			<div class="content">
				<div class="sub-header">Zettl forms</div>
				<div class="">
					<button data-toggle="zefflmodal" data-target="#new_form_modal" class="button">New form</button>
				</div>
			</div>	
		</div>
	</div>
	<div class='row'>
		<div class=' column'>
			<h2><?php _e('All Forms','zeffl_basic'); ?> <span>(<?php echo $total_forms; ?>)</span></h2>
			<div class='block'>
				<div class='table_list form_list' cellpadding='0' cellspacing='0'>
					<div class='tr thead row'>
						<span class="column large-1">ID</span>
						<span class="column large-2">Name</span>
						<span class="column large-4"> Short code</span>
						<span class="column large-1">Views</span>
						<span class="column large-2">Last Edit</span>
						<span class="column large-2"></span>
					</div>
					<div class='tbody'>
						<?php
						if ( $total_forms>0 )
						{
							foreach ($forms as $key => $value) {
								$form_views = $wpdb->get_var( "SELECT SUM(views) FROM $views_table WHERE form = '$value->id'" );
								$form_views = $form_views==0 ? '0' : $form_views;
								?>
								<div class='tr form-<?php echo $value->id; ?> row'>
									<span class="column large-1"><a href='admin.php?page=zeffl_basic_dashboard&id=<?php echo $value->id; ?>'><?php echo $value->id; ?></a></span>
									<span class="column large-2"><a href='admin.php?page=zeffl_basic_dashboard&id=<?php echo $value->id; ?>'><?php echo $value->name; ?></a></span>
									<span class="column large-4"><input type="text" value="[zeffl id='<?php echo $value->id; ?>' align='center'][/zeffl]"/></span>
									<span class="column large-1"><a href='admin.php?page=zeffl_basic_dashboard&id=<?php echo $value->id; ?>'><?php echo $form_views; ?></a></span>
									<span class="column large-2"><a href='admin.php?page=zeffl_basic_dashboard&id=<?php echo $value->id; ?>'><?php echo zeffl_time_ago(strtotime(current_time('mysql'))-strtotime($value->modified)); ?></a></span>
									<span class="column large-2"><i data-id='<?php echo $value->id; ?>' class='trash-icon trash-form icon-trash-1'></i></span>
								</div>
								<?php
							}
						}
						else
						{
							?>
							<span class='no-subs-content' data-toggle="zefflmodal" data-target="#new_form_modal"><?php _e('Create New Form','zeffl_basic'); ?></span>
							<?php
						}
						?>
					</div>
				</div>
			</div>
		</div> 
	</div>
	<div class="zefflmodal zefflfade" id="new_form_modal">
		<div class="zefflmodal-dialog" style="width: 350px">
			<form class="zefflmodal-content" id='new_form'>
			<div class='zefflmodal-header'>
					<button class='zefflclose' type="button" class="close" data-dismiss="zefflmodal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<span class="active"><?php _e('New Form','zeffl_basic'); ?></span>
					<nav class='nav-tabs' data-content='#add_form_tabs'>
						

					</nav>
			</div>
				<div class="zefflmodal-body">
					<input type='text' name='form_name' placeholder='<?php _e('Form Name','zeffl_basic'); ?>'>

					<div class="zefflmodal-section">
						<button type="submit" class="button blue wide"><span><?php _e('Create Form','zeffl_basic'); ?></span><span class="zeffl-spinner small">
							<span class="bounce1"></span>
							<span class="bounce2"></span>
							<span class="bounce3"></span>
						</span></button>
						<span class='response'></span>
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="zefflmodal zefflfade" id="submission_modal">
		<div class="zefflmodal-dialog">
			<div class="zefflmodal-content">
				<div class="zefflmodal-header">
					<button class='zefflclose' type="button" class="close" data-dismiss="zefflmodal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="zefflmodal-title"></h4>
				</div>
				<div class="zefflmodal-body">
					BODY
				</div>
				<div class="zefflmodal-footer">
					<button class="button blue small close"><?php _e('Close','zeffl_basic'); ?></button>
				</div>
			</div>
		</div>
	</div>

</div>