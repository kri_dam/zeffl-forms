<?php
defined( 'ABSPATH' ) or die( 'Cheating, huh?' );
global $zeffl_version, $wpdb, $forms_table;
$form_id = intval($_GET['id']);
$qry = $wpdb->get_results( "SELECT * FROM $forms_table WHERE id = '$form_id'" );
?>
<input type='hidden' id='form_id' value='<?php echo $form_id; ?>'>
<div ng-app='zeffl'>
	<div class='zeffl-forms-css' ng-controller='FormController' ng-init='AngularInit()'>
		<div class='options-head'>
			
			<div style="float:left">
				<a href='admin.php?page=zeffl_basic_dashboard' class='button blue'>Back</a>
				<div class='add_fields'>
					<button class='button blue icon-{{Options.show_fields}}' ng-click='Options.show_fields = !Options.show_fields'><?php _e('Add Field','zeffl_basic') ?></button>
					<div ng-show='Options.show_fields' class='ng-hide'>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("oneLineText")'><?php _e('One Line Input','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("email")'><?php _e('Email Input','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("textarea")'><?php _e('Comment Box','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("checkbox")'><?php _e('Checkboxes','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("dropdown")'><?php _e('Dropdown','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("customText")'><?php _e('Custom Text','zeffl_basic') ?></div>
						<div ondragstart="drag(event)" ondragend="dragEnd(event)" dnd-draggable class='button' ng-click='addFormElement("submit")'><?php _e('Submit','zeffl_basic') ?></div>
					</div>
				</div>
				<div>
					<button data-toggle="zefflmodal" data-target="#form_options_modal" class='button blue'> <?php _e('Form Options','zeffl_basic') ?></button>
				</div>
			</div>

			<div style="float:right">
				<div>
					<button id='form_save_button' ng-click='saveForm()' class='button blue'><span class="zeffl-spinner small"><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></span><span class='one'> <?php _e('Save all changes','zeffl_basic') ?></span></button>
				</div>
				<div>
					<button type='submit' ng-click='saveForm("preview")' id='form_preview_button' class='button blue'><?php _e('Preview','zeffl_basic') ?></button>
				</div>
			</div>
		</div>
		<div class='form-cover-builder hide-form'>
			<span class='zeffl-spinner zeffl-spinner-form small'><div class='bounce1'></div><div class='bounce2'></div><div class='bounce3'></div></span>
			<div id='form-cover-html' class='nos-{{Builder.FormElements.length}}' style='width: {{Builder.form_width_nos}}px'>
				<!--RFH-->
				<div class='no-fields' ng-click='Options.show_fields = true'><?php _e('+ Add a Field','zeffl_basic'); ?></div>
				<div style='width: {{Builder.form_width}}' class='form_width_change'><span><?php _e('Width','zeffl_basic') ?></span><input ng-model='Builder.form_width' type='text'/><i data-html='true' data-toggle='tooltip' title='<?php _e('You should use a value like <Strong>300px</strong>, or <Strong>420px</strong> here.<br>You can also define a %-based width, like <strong>50%</strong>, which will set the width of the form to 50% the width of its container. ','zeffl_basic') ?>' class='fa fa-question-circle'></i></div>
				<!--RTH--><form ondrop="drop(event)" ondragover="allowDrop(event)" dnd-list='Builder.FormElements' data-id='<?php echo $form_id; ?>' class='zeffl_form align-{{Builder.form_align}} frame-{{Builder.form_frame}}' style='width: {{Builder.form_width}}; background-color: {{Builder.form_background}}; color: {{Builder.font_color}}; font-size: {{Builder.font_size}}%'><div ng-class-odd="'odd'" ng-class='["form-element", "form-element-"+element.elementDefaults.identifier, "options-"+element.show_options, "index-"+element.show_options]' ng-class-even="'even'" ng-repeat='element in Builder.FormElements' index='{{$index}}' style='width: {{element.elementDefaults.field_width}}'><span class='error'></span><div ng-click='element.show_options = !element.show_options' class='form-element-html' compile='element.element'></div><!--RFH--><span class='options-panel' ondragstart="dragStart(event)" dnd-draggable='element' ondrag="setHeight(event)" dnd-moved='Builder.FormElements.splice($index, 1);' ondragend='removeAnimate()'><i class='fa fa-arrows'></i></span><div ng-show='element.show_options' class='form-options'><div class='sub-options'><div title='Field ID'>{{element.elementDefaults.identifier}}</div><span title='Delete Field' ng-click='removeFormElement($index)' class='delete'><i class="fa fa-trash" aria-hidden="true"></i></span><span title='Duplicate Field' ng-click='duplicateFormElement($index)' class='duplicate'><i class='fa fa-clone'></i></span></div><div class='options-main' compile='element.elementOptions'></div></div><!--RTH--></div></form>
			</div>
		</div>

		<div class="zefflmodal zefflfade" id="form_options_modal">
			<div class="zefflmodal-dialog">
				<div class="zefflmodal-content">
					<div class="zefflmodal-header">
						<button class='zefflclose' type="button" class="close" data-dismiss="zefflmodal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="zefflmodal-title"><?php _e('Form Options','zeffl_basic'); ?></h4>
						<nav class='nav-tabs' data-content='#options_tabs'>
							<span class='active'><?php _e('Styling','zeffl_basic'); ?></span>
							<span><?php _e('Embed','zeffl_basic'); ?></span>
							<span><?php _e('Others','zeffl_basic'); ?></span>
							<span><?php _e('Integrations','zeffl_basic'); ?></span>
						</nav>
					</div>
					<div class="zefflmodal-body">
						<div id='options_tabs' class='nav-content'>
							<div class='active'>
								<div class='group'>
									<h2>
										<?php _e('Form Styling','zeffl_basic'); ?>
									</h2>
									<div>
										<span class='option-label'><?php _e('Form Background','zeffl_basic'); ?></span>
										<input type="text" value="#fff" class="color-picker" ng-model='Builder.form_background'>
									</div>
									<div>
										<span class='option-label'><?php _e('Font Size','zeffl_basic'); ?></span>
										<button class='button' ng-click='Builder.font_size = Builder.font_size + 5'>+</button>
										<button class='button' ng-click='Builder.font_size = Builder.font_size - 5'>-</button>
										&nbsp; {{Builder.font_size}}%
									</div>
									<div>
										<span class='option-label'><?php _e('Font Color','zeffl_basic'); ?></span>
										<input type="text" value="#fff" class="color-picker" ng-model='Builder.font_color'>
									</div>
									<div>
										<label>
											<span class='option-label'>Form Frame</span>
											<input type='checkbox' ng-model='Builder.form_frame' ng-true-value='"hidden"' ng-false-value='"visible"'> <?php _e('Remove','zeffl_basic'); ?>
										</label>
									</div>
								</div>
							</div>
							<div>
								<div class='group'>
									<h2>
										<?php _e('Dedicated Form Page','zeffl_basic'); ?>
									</h2>
									<div>
										<span class='option-label'><?php _e('Form Link','zeffl_basic'); ?></span>
										<textarea onclick='select()' style='width: 350px' rows='1' class='copy-code' readonly><?php echo get_site_url().'/form/'.$form_id; ?></textarea>
									</div>
									<div>
										<span class='option-label'><?php _e('Public Access','zeffl_basic'); ?></span>
										<label><input type='checkbox' ng-model='Builder.Config.disable_form_link'/> Disable</label>
									</div>
								</div>
								<div class='group'>
									<h2>
										<?php _e('Using Shortcode','zeffl_basic'); ?>
									</h2>
									<div>
										<span class='option-label'><?php _e('Form Alignment','zeffl_basic'); ?></span>
										<label><input type='radio' ng-model='temp_align' name='temp_form_align' value='left'><?php _e('Left','zeffl_basic'); ?></label>&nbsp;
										<label><input type='radio' ng-model='temp_align' name='temp_form_align' value='center'><?php _e('Center','zeffl_basic'); ?></label>&nbsp;
										<label><input type='radio' ng-model='temp_align' name='temp_form_align' value='right'><?php _e('Right','zeffl_basic'); ?></label>
									</div>
									<div>
										<span class='option-label'><?php _e('Shortcode','zeffl_basic'); ?></span>
										<textarea onclick='select()' style='width: 350px' rows='1' class='copy-code' readonly>[zeffl id='<?php echo $form_id; ?>' align='{{temp_align}}'][/zeffl]</textarea>
									</div>
								</div>
							</div>
							<div>
								<div class='group'>
									<h2>
										<?php _e('Custom Messages','zeffl_basic'); ?>
									</h2>
									<div>
										<span class='option-label'><?php _e('Form Sent','zeffl_basic'); ?></span>
										<input type='text' ng-model='Builder.Config.messages.form_sent' style="width: 365px">
									</div>
									<div>
										<span class='option-label'><?php _e('Validation Errors','zeffl_basic'); ?></span>
										<input type='text' ng-model='Builder.Config.messages.form_errors' style="width: 365px">
									</div>
								</div>
							</div>
							<div>
								<div class='group'>
									<h2>
										<?php _e('Integrations','zeffl_basic'); ?>
									</h2>
									<div>
										<span class='option-label'><?php _e('Integrate with','zeffl_basic'); ?></span>
										<label><input type='radio' ng-model='Builder.Config.formHandlers.integration' value="fs"/>Freshsales</label>			
										<label><input type='radio' ng-model='Builder.Config.formHandlers.integration' value="fm"/>FreshMarketer</label>										
									</div>
									<div>
										<span class='option-label'><?php _e('URL','zeffl_basic'); ?></span>
										<input type='url' ng-model='Builder.Config.formHandlers.url' style="width: 365px">
									</div>
									<div>
										<span class='option-label'><?php _e('API key','zeffl_basic'); ?></span>
										<input type='text' ng-model='Builder.Config.formHandlers.api' style="width: 365px">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- /.zefflmodal-content -->
			</div><!-- /.zefflmodal-dialog -->
		</div><!-- /.zefflmodal -->
	</div>
</div>
<?php

?>