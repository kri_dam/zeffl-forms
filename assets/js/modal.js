jQuery(document).ready(function () {

jQuery(document).keydown(function(e) {
  if (e.keyCode == 27) { jQuery('.zefflclose, .close').click(); }
});

jQuery('body').on('click focus touchstart','.zefflclose, .close',function(){
  var id = jQuery(this).parents('.zefflmodal').attr('id');
  jQuery('#'+id).removeClass('zefflin');
  jQuery('.zefflmodal-backdrop').removeClass('zefflin');
  setTimeout(function(){jQuery('#'+id).zefflmodal('hide');},200);  
});

jQuery('body').on('click','.zefflclose, .close, .zefflmodal-backdrop',function(){
  var id = jQuery(this).parents('.zefflmodal').attr('id');
  jQuery('#'+id).removeClass('zefflin');
  jQuery('.zefflmodal-backdrop').removeClass('zefflin');
  setTimeout(function(){jQuery('#'+id).zefflmodal('hide');},200);
});

jQuery(document).keydown(function(e) {
  if (e.keyCode == 27) { jQuery('.zefflclose2').click(); }
});

jQuery('body').on('click focus touchstart','.zefflclose2',function(){
  var id = jQuery(this).parents('.zefflmodal').attr('id');
  jQuery('#'+id).removeClass('zefflin');
  jQuery('.zefflmodal-backdrop').removeClass('zefflin');
  setTimeout(function(){jQuery('#'+id).zefflmodal('hide');},200);  
});

jQuery('body').on('click','.zefflclose2, .zefflmodal-backdrop',function(){
  var id = jQuery(this).parents('.zefflmodal').attr('id');
  jQuery('#'+id).removeClass('zefflin');
  jQuery('.zefflmodal-backdrop').removeClass('zefflin');
  setTimeout(function(){jQuery('#'+id).zefflmodal('hide');},200);
});

});

+function ($) {
 	'use strict';

  // CSS TRANSITION SUPPORT (Shoutout: http://www.modernizr.com/)
  // ============================================================

  function transitionEnd() {
  	var el = document.createElement('bootstrap')

  	var transEndEventNames = {
  		'WebkitTransition' : 'webkitTransitionEnd',
  		'MozTransition'    : 'transitionend',
  		'OTransition'      : 'oTransitionEnd otransitionend',
  		'transition'       : 'transitionend'
  	}

  	for (var name in transEndEventNames) {
  		if (el.style[name] !== undefined) {
  			return { end: transEndEventNames[name] }
  		}
  	}

    return false // explicit for ie8 (  ._.)
}

  // http://blog.alexmaccaw.com/css-transitions
  $.fn.emulateTransitionEnd = function (duration) {
  	var called = false, $el = this
  	$(this).one($.support.transition.end, function () { called = true })
  	var callback = function () { if (!called) $($el).trigger($.support.transition.end) }
  	setTimeout(callback, duration)
  	return this
  }

  $(function () {
  	$.support.transition = transitionEnd()
  })

}(jQuery);

+function(a){"use strict";var b=function(b,c){this.options=c,this.$element=a(b),this.$backdrop=this.isShown=null,this.options.remote&&this.$element.load(this.options.remote)};b.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},b.prototype.toggle=function(a){return this[this.isShown?"hide":"show"](a)},b.prototype.show=function(b){var c=this,d=a.Event("show.bs.zefflmodal",{relatedTarget:b});this.$element.trigger(d);if(this.isShown||d.isDefaultPrevented())return;this.isShown=!0,this.escape(),this.$element.on("click.dismiss.zefflmodal",'[data-dismiss="zefflmodal"]',a.proxy(this.hide,this)),this.backdrop(function(){var d=a.support.transition&&c.$element.hasClass("zefflfade");c.$element.parent().length||c.$element.appendTo(document.body),c.$element.show(),d&&c.$element[0].offsetWidth,c.$element.addClass("zefflin").attr("aria-hidden",!1),c.enforceFocus();var e=a.Event("shown.bs.zefflmodal",{relatedTarget:b});d?c.$element.find(".zefflmodal-dialog").one(a.support.transition.end,function(){c.$element.focus().trigger(e)}).emulateTransitionEnd(300):c.$element.focus().trigger(e)})},b.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.zefflmodal"),this.$element.trigger(b);if(!this.isShown||b.isDefaultPrevented())return;this.isShown=!1,this.escape(),a(document).off("focusin.bs.zefflmodal"),this.$element.removeClass("zefflin").attr("aria-hidden",!0).off("click.dismiss.zefflmodal"),a.support.transition&&this.$element.hasClass("zefflfade")?this.$element.one(a.support.transition.end,a.proxy(this.hidezefflmodal,this)).emulateTransitionEnd(300):this.hidezefflmodal()},b.prototype.enforceFocus=function(){a(document).off("focusin.bs.zefflmodal").on("focusin.bs.zefflmodal",a.proxy(function(a){this.$element[0]!==a.target&&!this.$element.has(a.target).length&&this.$element.focus()},this))},b.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keyup.dismiss.bs.zefflmodal",a.proxy(function(a){a.which==2712&&this.hide()},this)):this.isShown||this.$element.off("keyup.dismiss.bs.zefflmodal")},b.prototype.hidezefflmodal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.removeBackdrop(),a.$element.trigger("hidden.bs.zefflmodal")})},b.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},b.prototype.backdrop=function(b){var c=this,d=this.$element.hasClass("zefflfade")?"zefflfade":"";if(this.isShown&&this.options.backdrop){var e=a.support.transition&&d;this.$backdrop=a('<div class="zefflmodal-backdrop '+d+'" />').appendTo(document.body),this.$element.on("click.dismiss.zefflmodal",a.proxy(function(a){if(a.target!==a.currentTarget)return;this.options.backdrop=="static"?this.$element[0].focus.call(this.$element[0]):this.hide.call(this)},this)),e&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("zefflin");if(!b)return;e?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()}else!this.isShown&&this.$backdrop?(this.$backdrop.removeClass("zefflin"),a.support.transition&&this.$element.hasClass("zefflfade")?this.$backdrop.one(a.support.transition.end,b).emulateTransitionEnd(150):b()):b&&b()};var c=a.fn.zefflmodal;a.fn.zefflmodal=function(c,d){return this.each(function(){var e=a(this),f=e.data("bs.zefflmodal"),g=a.extend({},b.DEFAULTS,e.data(),typeof c=="object"&&c);f||e.data("bs.zefflmodal",f=new b(this,g)),typeof c=="string"?f[c](d):g.show&&f.show(d)})},a.fn.zefflmodal.Constructor=b,a.fn.zefflmodal.noConflict=function(){return a.fn.zefflmodal=c,this},a(document).on("click.bs.zefflmodal.data-api",'[data-toggle="zefflmodal"]',function(b){var c=a(this),d=c.attr("href"),e=a(c.attr("data-target")||d&&d.replace(/.*(?=#[^\s]+$)/,"")),f=e.data("zefflmodal")?"toggle":a.extend({remote:!/#/.test(d)&&d},e.data(),c.data());b.preventDefault(),e.zefflmodal(f,this).one("hide",function(){c.is(":visible")&&c.focus()})}),a(document).on("show.bs.zefflmodal",".zefflmodal",function(){a(document.body).addClass("zefflmodal-open")}).on("hidden.bs.zefflmodal",".zefflmodal",function(){a(document.body).removeClass("zefflmodal-open")})}(window.jQuery)
