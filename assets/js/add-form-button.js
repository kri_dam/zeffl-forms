jQuery(document).ready(function(){

	jQuery('body').on('submit','#zeffl_add_form_modal > form',function(event){
		event.preventDefault();
		var form = jQuery('[name="zeffl_form_id"]:checked').val();
		var align = jQuery('[name="zeffl_form_align"]:checked').val();
		var code = "[zeffl id='"+form+"' align='"+align+"'][/zeffl]";
		jQuery('#zeffl_add_form_modal').zefflmodal('hide');
		tinymce.execCommand('mceInsertContent', 0, code);
	});

});